function [ cvx_optval, C, cvx_cputime, cvx_status, verify ] = sos_cvx( A, b )
%SOS_CVX optimise polynomial via SOS
%
%	Call: [ cvx_optval, C, cvx_cputime, status ] = sos_cvx( A, b )
%	Input:
%	  - A: exponent matrix of the polynomial,
%		first row must be one, first column otherwise zero
%	  - b: vector of coefficients, corresponding to the columns of A
%	Output:
%	  - cvx_optval: how much has to be added to be an SOS
%	  - C: SDP matrix, such that Z^T * C * Z is the polynomial (A,b), where
%		Z is the vector of all monomials
%	  - cvx_cputime: running time of the solver
%	  - verify: 1 = solved, -1 = failed, 0 = inaccurate/else
%	  - status: status message of the solver
%
% - Example input -
% A = [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
%      [0, 12, 0, 0, 1, 2, 2, 3, 3, 3, 6, 7];
%      [0, 0, 12, 0, 2, 1, 3, 1, 4, 5, 1, 1];
%      [0, 0, 0, 12, 3, 2, 5, 2, 2, 2, 4, 2]];
% b = [0.4167578474054706, 0.056266827226329474, 2.136196095668454, 1.6402708084049886, -1.7934355851948631, -0.841747365656204, -0.5028814171580428, -1.2452880866072316, -1.057952218862339, -0.9090076149268493, -0.5514540445464243, -2.2922080128149576];

	A = A(2:end,:);
	n = size(A,1);
	d = max(sum(A,1)) / 2;
	largesize = nchoosek(n + 2*d, n);
	smallsize = nchoosek(n + d, n);
	%set the coefficients for monomials up to degree d
	coefficients = zeros(1,largesize);
	for i = 1:size(A,2)
		coefficients(vector_to_index(A(:,i),2*d) + 1) = coefficients(vector_to_index(A(:,i),2*d) + 1) + b(i);
	end

	cvx_begin sdp quiet
	% % %     optimization variables
	variable C(smallsize,smallsize) semidefinite
	variable gam
	% % %     objective function
	minimise gam
	% % %     constraints
	subject to
	C(1,1) == coefficients(1) + gam;
	for i = 2:largesize
		alpha = index_to_vector(i-1,n,2*d);
		summands = smaller_vectors(alpha,sum(alpha)-d,d);
		%define auxiliary expression 'line'
		expression line(size(summands,2));
		for j=1:size(summands,2)
			line(j) = C(vector_to_index(summands(:,j),d)+1,vector_to_index(alpha-summands(:,j),d)+1);
		end
		sum(line) == coefficients(i);
	end
	cvx_end
	
	if strcmp(cvx_status, 'Failed')
		verify = -1;
	else
		verify = 0;
	end
	C = full(C);
end

function [ vector ] = index_to_vector( index,n,d )
%INDEX_TO_VECTOR Computed the index-th exponent vector in [0..d]^n
	if n == 1
		vector = index;
		return
	end
	vector = zeros(n,1);
	i = 0;
	step = nchoosek(n-1+d-i,n-1);
	while index >= step
		index = index - step;
		i = i + 1;
		step = nchoosek(n-1+d-i,n-1);
	end
	vector(1) = i;
	vector(2:end) = index_to_vector(index, n - 1, d - i);
end

function [ res ] = vector_to_index( vector, d )
%VECTOR_TO_INDEX compute the index of the given vector in [0..d]^n
	if ~any(vector)
		res = 0;
		return
	end
	n = length(vector);
	offset = zeros(vector(1),1);
	for i=1:vector(1)
		offset(i) = nchoosek(n+d-i,n-1);
	end
	res = sum(offset) + vector_to_index(vector(2:end), d - vector(1));
end

function [ res ] = smaller_vectors( alpha, mindegree, maxdegree )
%SMALLER_VECTORS compute all elementwise smaller vector, in given bounds
	if sum(alpha) < mindegree;res = [];return;end
	if maxdegree == 0;res = zeros(length(alpha),1);return;end
	if length(alpha) == 1; res = max(0,mindegree):min(alpha(1), maxdegree); return; end

	res = [];
	for i = 0:min(alpha(1), maxdegree)
		sub = smaller_vectors(alpha(2:end), mindegree - i, maxdegree - i);
		res = horzcat(res, vertcat(i * ones(1,size(sub,2)),sub));
	end
end