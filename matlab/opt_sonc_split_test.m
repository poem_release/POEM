clear;
cvx_solver sedumi;

%Test a single polynomial with different triangulations

A = transpose([1 0 0; 1 4 0 ; 1 0 2; 1 2 4; 1 4 4; 1 1 1; 1 1 2; 1 2 3; 1 3 2]);
b = [1 1 1 1 1 -1 -1 -1 -1];
T1 = {[1 2 4 6 8]; [1 3 5 7]; [1 2 5 9]};
T2 = {[1 2 4 6 8]; [1 3 5 7]; [1 2 5 9]; [1 2 3 6]};
T3 = {[1 2 4 6 8]; [1 3 5 7]; [1 2 5 9]; [1 2 3 6]; [1 4 5 8]};

[ opt_even_1, C1, time_even_1, status1 ] = opt_sonc_split_even( A, b, T1 );
[ opt_even_2, C2, time_even_2, status2 ] = opt_sonc_split_even( A, b, T2 );
[ opt_even_3, C3, time_even_3, status3 ] = opt_sonc_split_even( A, b, T3 );

%Due to the even split, the third triangulation performs worse than the second.
% opt1 == -0.2449
% opt2 == -0.4006
% opt3 == -0.0936

[ opt_outer_1, C1, time_outer_1, status1 ] = opt_sonc_split_outer( A, b, T1 );
[ opt_outer_2, C2, time_outer_2, status2 ] = opt_sonc_split_outer( A, b, T2 );
[ opt_outer_3, C3, time_outer_3, status3 ] = opt_sonc_split_outer( A, b, T3 );

%Since we split the inner points evenly, this function is not monotone in T.
%But each result is better than the even split of all points.
% opt1 == -0.3071
% opt2 == -0.5058
% opt3 == -0.4705

T4 = {[1 2 4 6 8]; [1 3 5 7]; [1 2 5 9]};
T5 = {[1 2 4 8]; [1 3 5 7]; [1 2 5 9]; [1 2 3 6]};
T6 = {[1 3 5 7]; [1 2 5 9]; [1 2 3 6]; [1 4 5 8]};

T = T6;
[ opt_even, C_even, time_even, status_even ] = opt_sonc_split_even( A, b, T );
[ opt_out, C_out, time_out, status_out] = opt_sonc_split_outer( A, b, T );
[ opt_casc, C_casc, time_casc, status_casc ] = opt_sonc_split_cascade( A, b, T );

% opt_casc == -0.1328
% opt_even == -0.3594
% opt_out  == -0.4736

%Instead of an even split, we have a variable split of the outer
%coefficients, thus increasing the problem.
%We get a better result at the cost of higher running time.
%The cascading solution lies in between with its time, but often has worse
%results than the even split.

A = [[1 1 1 1 1 1 1 1]; [0 4 6 4 0 1 2 3]; [0 0 4 6 4 1 3 2]];
b = [1 4 2 1 3 -1 -2 -1];
T_c1 = {[1 2 5 6]; [2 4 5 7 8]; [2 3 5 7 8]};
T_c2 = {[1 2 5 6]; [2 3 5 7 8]; [2 4 5 7 8]};

[ opt_c1, C, time_c1, status ] = opt_sonc_split_cascade( A, b, T_c1 );
[ opt_c2, C, time_c2, status ] = opt_sonc_split_cascade( A, b, T_c2 );

%The order of the covering does matter, even though the difference here is
%small.
% opt_c1 = -0.7001
% opt_c2 = -0.6994