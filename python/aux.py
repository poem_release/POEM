#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""Define constants and basic functions."""

import numpy as np
import collections
from math import gcd # Python versions 3.5 and above
#from fractions import gcd # Python versions below 3.5
from functools import reduce # Python version 3.x
from scipy.special import binom
from typing import Any, Iterable, TypeVar, Union, Callable
import datetime
try:
	import sympy
	sympy_flag = True
except (ModuleNotFoundError, ImportError):
	sympy_flag = False

# ===  Basic Constants ===

VERBOSE = False
SAVE_PATH = '../instances/'
DB_NAME = 'runs.db'
DIGITS = 23
EPSILON = 2**-DIGITS
FAULT_DATA = {'C':np.array([[]]), 'opt': np.inf, 'solver_time': 0, 'time': 0, 'verify': -1, 'status':'no solution'}
GP_SOLVER = 'ECOS'
SDP_SOLVER = 'MOSEK'

Number = TypeVar('Nr', float, np.float, int, sympy.Rational)
Vector = Iterable[Number]
Int_Vector = Iterable[int]
Matrix = Iterable[Iterable[Number]]

# === Auxiliary Functions ===

symlog = np.vectorize(sympy.log)

def parse(number: str) -> Number:
	"""Parse a number from string to the correct datatype.

	Call:
		res = parse(number)
	Input:
		number: string, representing a number
	Output:
		res: number as int/float/sympy.Rational
	"""
	try:
		res = int(number)
	except ValueError:
		try:
			res = float(number)
		except ValueError:
			res = sympy.sympify(number)
	return res

def get_type(array: Iterable[Any]):
	"""Determine common data type for given arrray.
	
	Possible results: int, np.float, sympy.Rational, object
	"""
	for type_attempt in [int, np.float, sympy.Rational]:
		if all([isinstance(entry, type_attempt) for entry in array]):
			return type_attempt
	return object

def bitsize(arg: Union[Iterable[Number], Number]) -> int:
	"""Compute the bit size of a number or a collection of numbers.

	Call:
		size = bitsize(arg)
	Input:
		arg: number (int/np.int/float) or collection (np.array/list/...) in arbitrary nesting
	Output:
		size: (summed up) bit size of arg
	"""
	if isinstance(arg, collections.Iterable):
		return sum([bitsize(e) for e in arg])
	if type(arg) == int:
		return arg.bit_length()
	if type(arg) == np.int64:
		return int(arg).bit_length()
	if type(arg) == sympy.Rational:
		return arg.p.bit_length() + arg.q.bit_length()
	return 64 #default size for float and bounded integer

def binomial(n: int,k: int) -> int:
	"""Compute binomial coefficient as integer."""
	return int(binom(n,k))

def dt2sec(dt: datetime.timedelta) -> float:
	"""Convert a datetime.timedelta object into seconds.

	Call:
		res = dt2sec(dt)
	Parameters:
		dt: datetime.timedelta object
	Output:
		res: a float, representing the seconds of dt
	"""
	return dt.microseconds / 1000000.0 + dt.seconds
	
def _smaller_vectors(alpha: Int_Vector, mindegree: int = 0, maxdegree: int = -1) -> Iterable[Int_Vector]:
	"""For given vector alpha list all other vectors, which are elementwise smaller.

	Optionally these can be restricted to some maximal degree.

	Call:
		vector_list = _smaller_vectors_bound(alpha, mindegree, maxdegree)
	Input:
		alpha: vector of non-negative integers
		mindegree: non-negative integer
		maxdegree: non-negative integer
	Output:
		vector_list: list of lists, containing all vectors of degree between (including) `mindegree` and `maxdegree`, which are elementwise at most `alpha`
	"""
	if maxdegree == -1: maxdegree = sum(alpha)

	if type(alpha) != list:
		alpha = list(alpha)

	#base case
	if sum(alpha) < mindegree: return []
	if maxdegree == 0: return [[0 for _ in alpha]]
	if alpha == []: return [[]]
	
	return [[i] + l for i in range(0, min(alpha[0], maxdegree) + 1) for l in _smaller_vectors(alpha[1:], mindegree - i, maxdegree - i)]

def _vector_to_index(vector: Int_Vector, d: int) -> int:
	"""For some given exponent vector, compute the corresponding index in lexicographic order.
	
	This function is the inverse function to _index_to_vector.
	Call:
		index = _vector_to_index(vector, d)
	Input:
		vector: array of integers with sum at most d
		d: maximal degree
	Output:
		index: index of the given vector in the lexicographic ordering of `n`-dimensional vector with sum at most `d`
	"""
	#check for valid input
	if (vector < 0).any():
		raise TypeError('Negative entry.')
	if vector.sum() > d:
		raise TypeError('Entry too large.')
	#base case
	if not vector.any():
		return 0
	#compute first entry and recurse
	n = len(vector)
	offset = sum([binomial(n-1+d-i, n-1) for i in range(vector[0])])
	return offset + _vector_to_index(vector[1:], d - vector[0])

def _index_to_vector(index,n: int,d: int) -> Int_Vector:
	"""For some given index in lexicographic order, compute the corresponding exponent vector.

	Call:
		vector = _index_to_vector(index,n,d)
	Input:
		index: non-negative integer
		n: number of variables
		d: maximal degree
	Output:
		vector: array of length `n`, the `index`-th exponent vector in the lexicographic ordering of `n`-dimensional vectors with sum at most `d`
	"""
	#base case
	if n == 1:
		return np.array([index])
	vector = np.zeros(n, dtype=np.int)

	#subtract binomial coefficients as long as possible, yields first entry, then recursion
	i = 0
	step = binomial(n-1+d-i,n-1)
	while index >= step:
		index -= step
		i += 1
		step = binomial(n-1+d-i,n-1)
	vector[0] = i
	vector[1:] = _index_to_vector(index, n-1, d-i)
	return vector

def is_psd(C: Matrix) -> bool:
	"""Check whether matrix C is positive semidefinite, by checking the eigenvalues."""
	try:
		return all(np.linalg.eigvalsh(C) >= -EPSILON)
	except LinAlgError:
		return False

def linsolve(A: Matrix, b: Vector) -> Vector:
	"""Solve a linear equation system for a possible singular matrix."""
	if b.size == 0:
		return np.array([])
	return np.linalg.lstsq(A,b,rcond = None)[0]

def unify_status(status: Union[str,int,Iterable[str],Iterable[int]]) -> int:
	"""Give a uniform representation for different status flags.

	The following are equivalent:
	1 = Solved = optimal
	0 = Inaccurate = optimal_inaccurate
	-1 = no solution
	"""
	if type(status) == list:
		for i in range(len(status)):
			if status[i] in ['Solved', 'optimal', 1]:
				status[i] = 1
			elif status[i] in ['no solution', -1]:
				status[i] = -1
			else: status[i] = 0
		return min(status)
	elif status in ['Solved', 'optimal']:
		return 1
	elif status == 'no solution':
		return -1
	else:
		return 0

def maximal_elements(input_list: Iterable[Vector], comp: Callable[[Vector, Vector], bool] = (lambda u,v: (u <= v).all()), sort_key: Callable[[Vector], Number] = np.sum) -> Iterable[Vector]:
	"""Given a list of np.array, compute the maximal elements.

	Call:
		maximal = maximal_elements(input_list)
	Input:
		input_list: list of np.array, all need to have the same length.
	Output:
		maximal: list of np.array, which contains the maximal elements of input_list,
			the order used is elementwise "<="
	"""
	l = input_list.copy()
	if sort_key is not None:
		l.sort(reverse=True, key = sort_key)
	maximal = [l[0]]
	for e in l[1:]:
		i = 0
		replaced = False
		while i < len(maximal):
			if comp(maximal[i], e):
			#if (maximal[i] <= e).all():
				if replaced:
					maximal.pop(i)
					i -= 1
				else:
					maximal[i] = e
					replaced = True
			#if (e <= maximal[i]).all():
			if comp(e, maximal[i]):
				break
			i += 1
		if i == len(maximal) and not replaced:
			maximal.append(e)
	return maximal	

def flatten(l: Iterable) -> Iterable:
	"""Flatten a list of irregular depth to a generator."""
	#from https://stackoverflow.com/questions/2158395/flatten-an-irregular-list-of-lists
	for el in l:
		if isinstance(el, collections.Iterable) and not isinstance(el, (str, bytes)):
			yield from flatten(el)
		else:
			yield el

#alternative, check which one is faster
def flatten2(l: Iterable) -> Iterable:
	"""Flatten a list of irregular depth to a list."""
	#from https://stackoverflow.com/questions/2158395/flatten-an-irregular-list-of-lists
	if isinstance(l, collections.Iterable):
		return [a for i in l for a in flatten2(i)]
	else:
		return [l]

def lcm(denominators : Int_Vector) -> int:
	"""Compute the least common multiple of a list."""
	if denominators == []: return 1
	return reduce(lambda a,b: a*b // gcd(a,b), denominators)

def to_fraction(number: Number, eps: Number = EPSILON, bound: int = 0) -> sympy.Rational:
	"""Round a given number to a fraction with given accuracy.

	Call:
		frac = to_fraction(number[, eps][, bound])
	Input:
		number: float/algebraic number (any arithmetic expression that can be handled by sympy), number to be converted into a fraction
		eps [optional, default: aux.EPSILON]: desired absolute accuracy
		bound [optional, default: 0]: number, in which direction to round
			0 : rounded down
			1 : rounded up
	Output:
		frac: symbolic fraction, such that |frac - number| < eps
	"""
	if bound == 1:
		return sympy.ceiling(number * 2**DIGITS) / 2**DIGITS
	elif bound == 0:
		return sympy.floor(number * 2**DIGITS) / 2**DIGITS
	else:
		raise Exception('Rounding: bound = %d' % bound)
