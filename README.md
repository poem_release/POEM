Polynomial Optimisation
=======================

# Content

* Functions to compute a lower bound for a polynomial
* Computation via sums of squares (SOS) and sums of non-negative circuit polynomials (SONC)
* both methods written in Matlab and Python
* Polynomial class, to have a simple interface
* Functions to generate different types of polynomials, see below

* sql code to create the data base

# Usage

* At first start, move to folder `python` and run `setup.py` to make sure, all packages are installed and to create documentation in html.
* Create polynomial instance
	* matrix of exponents, vector fo coefficients
	* from a string
	* create new random instance
* optimise with chosen method, result contains lower bound and certificate for this bound (decomposition as SOS/SONC)

Every public function has a docstring with its description, including arguments and output.
Also see the html-documentation.

## Example Code
```python
from polynomial import *

#create instance from matrix and vector/list
A = np.array([[1, 1, 1, 1],[0, 2, 4, 2],[0, 4, 2, 2]])
b = [1, 1, 1, -3]
p1 = Polynomial(A,b)

#create instance from string, string can e.g. be taken from symbolic expression in Matlab or sympy
import sympy
x = sympy.IndexedBase('x')
p2 = Polynomial(str(8*x[0]**6 + 6*x[1]**6 + 4*x[2]**6+2*x[3]**6 -3*x[0]**3*x[1]**2 + 8*x[0]**2*x[1]*x[2]*x[3] - 9*x[1]*x[3]**4 + 2*x[0]**2*x[1]*x[3] - 3*x[1]*x[3]**2 + 1))

#in strings you can use round brackets, squares brackets or no brackets at all
p2 = Polynomial('8*x(0)**6 + 6*x(1)**6 + 4*x(2)**6+2*x(3)**6 -3*x(0)**3*x(1)**2 +  8*x(0)**2*x(1)*x(2)*x(3) - 9*x(1)*x(3)**4 + 2*x(0)**2*x(1)*x(3) - 3*x(1)*x(3)**2 + 1')

#create new random instance: shape, variables, degree, terms
p3 = Polynomial('standard_simplex',30, 60, 100, seed = 0)
p4 = Polynomial('simplex',3, 8, 28, seed = 4)
#general shape has additional input: minimum number of interior points
p5 = Polynomial('general',4,8,8,3,seed = 0)

#run chosen method
p2.sage_opt_python()
p3.sonc_opt_python()
p4.sos_opt_python()

#run all methods and display time and optimum
#should only take a few seconds on a modern machine, some more with Matlab installed 
p5.run_all()
p5.local_min(method = 'all')
p5.print_all()

```

## Running large test loop

* set `SAVE_PATH` in `aux.py`, to be the folder where the database will be stored
* go to folder 'python'
* make sure that the file 'status' just contains the string 'run' (with newline)
* start `runner.py -r`
* if you want to stop, just change the content of 'status' to anythin else (e.g. 'stop')
