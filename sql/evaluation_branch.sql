
select run.poly_id, variables, degree, terms, -opt, value, (value + opt) as difference_with_long_name, run.time, strategy, solver.name from polynomial, run, programme, solver, minimum where run.poly_id = polynomial.rowid and programme_id = programme.rowid and solver_id = solver.rowid and minimum.poly_id = polynomial.rowid and minimum.method = 'traverse';

create view traverse_fork_diff as select r1.poly_id as poly_id, variables, degree, terms, r1.low as traverse, r2.low as fork, r1.low - r2.low as diff from (select poly_id, max(-opt) as low from run, programme where strategy = 'traverse' and programme_id = programme.rowid group by poly_id) r1, (select poly_id, max(-opt) as low from run, programme where strategy = 'fork' and programme_id = programme.rowid group by poly_id) r2, polynomial where r1.poly_id = r2.poly_id and r1.poly_id = polynomial.rowid;

create view full_table as
	select p.poly_id, minimum.value, sonc_time, sonc_opt, sage_time, sage_opt, traverse_full_time, traverse_full_opt, traverse_sparse_time, traverse_sparse_opt, fork_sonc_time, fork_sonc_opt, fork_all_time, fork_all_opt from (select distinct poly_id from run) p 
	left outer join minimum on minimum.poly_id = p.poly_id
	left outer join (select poly_id, sum(time) as sonc_time, 				-min(opt) as sonc_opt from view_run_programme where strategy = 'sonc' group by poly_id) r1 on p.poly_id = r1.poly_id 
	left outer join (select poly_id, time as sage_time, 						-opt	as sage_opt from view_run_programme where strategy = 'sage') r2 on p.poly_id = r2.poly_id 
	left outer join (select poly_id, time as traverse_full_time, 		-opt	as traverse_full_opt from view_run_programme where strategy = 'traverse' and params like '%sparse": false%' ) r3 on p.poly_id = r3.poly_id 
	left outer join (select poly_id, time as traverse_sparse_time, 	-opt as traverse_sparse_opt from view_run_programme where strategy = 'traverse' and params like '%sparse": true%'	) r4 on p.poly_id = r4.poly_id 
	left outer join (select poly_id, time as fork_sonc_time, 				-opt	as fork_sonc_opt from view_run_programme where strategy = 'fork' 		and params like '%strategy": "sonc%'		) r5 on p.poly_id = r5.poly_id 
	left outer join (select poly_id, time as fork_all_time, 				-opt	as fork_all_opt from view_run_programme where strategy = 'fork' 		and params like '%strategy": "all%'			) r6 on p.poly_id = r6.poly_id 
;

select variables, terms, (round(100*avg(traverse_full_time)))/100 as time from (select distinct poly_id from run) p 
	left outer join polynomial on p.poly_id = polynomial.rowid
	left outer join (select poly_id, time as traverse_full_time, -opt as traverse_full_opt from view_run_programme where strategy = 'traverse' and params like '%sparse": false%' ) r3 on p.poly_id = r3.poly_id 
	group by variables, terms;

select variables, terms, (round(100*avg(fork_all_time)))/100 as time from (select distinct poly_id from run) p 
	left outer join polynomial on p.poly_id = polynomial.rowid
 	left outer join (select poly_id, time as fork_all_time, -opt as fork_all_opt from view_run_programme where strategy = 'fork' and params like '%strategy": "all%' ) r6 on p.poly_id = r6.poly_id 
	group by variables, terms;
