create table if not exists 
polynomial(
	shape text,
	variables integer not null,
	degree integer not null,
	terms integer not null,
	inner_terms integer,
	seed integer not null,
	json text unqiue --full JSON-string
);

create table if not exists 
solver(
	name text,
	version text,
	callname text not null
);

create table if not exists 
system(
	name text not null,
	RAM_kB integer,
	freq_GHz float,
	cores integer
);

create table if not exists 
programme(
	strategy text not null,
	language text not null,
	solver_id integer references solver(rowid)
);

create table if not exists 
run(
	verify integer, -- 1 = success, -1 = fail, 0 = unknown
	status text,
	time float,
	opt float,
	json text, --full JSON-string of computed values
	poly_id integer references polynomial(rowid),
	programme_id integer references programme(rowid),
	params text, 
	timestamp integer, 
	system_id integer references system(rowid)
);

insert into system (name, RAM_kB, freq_GHz, cores ) values ('Intel(R) Core(TM) i7-6700 CPU @ 3.40GHz', 16319092, 3.40, 8 );

insert into solver (name, version, callname) values ('SeDuMi','1.34','sedumi');
insert into solver (name, version, callname) values ('SDPT3','4.0','sdpt3');
insert into solver (name, version, callname) values ('SCS','1.2.7','SCS');
insert into solver (name, version, callname) values ('CVXOPT','1.1.9','CVXOPT');
insert into solver (name, version, callname) values ('ECOS','2.0.4','ECOS');
insert into solver (name, version, callname) values ('SOSTOOLS','3.0.1','sostools');
insert into solver (name, version, callname) values ('Gloptipoly','3','gloptipoly');
insert into solver (name, version, callname) values ('Trivial','0.1','trivial');


insert into programme (language, strategy, solver_id) values ('matlab','sos', (select rowid from solver where callname = 'sedumi'));
insert into programme (language, strategy, solver_id) values ('matlab','sos', (select rowid from solver where callname = 'sdpt3'));
insert into programme (language, strategy, solver_id) values ('matlab','sos', (select rowid from solver where callname = 'sostools'));
insert into programme (language, strategy, solver_id) values ('matlab','sos', (select rowid from solver where callname = 'gloptipoly'));
insert into programme (language, strategy, solver_id) values ('matlab','sonc',(select rowid from solver where callname = 'sedumi'));
insert into programme (language, strategy, solver_id) values ('matlab','sonc',(select rowid from solver where callname = 'sdpt3'));
insert into programme (language, strategy, solver_id) values ('python','sos', (select rowid from solver where callname = 'SCS'));
insert into programme (language, strategy, solver_id) values ('python','sonc',(select rowid from solver where callname = 'SCS'));
insert into programme (language, strategy, solver_id) values ('python','sonc',(select rowid from solver where callname = 'ECOS'));
insert into programme (language, strategy, solver_id) values ('python','trivial',(select rowid from solver where callname = 'trivial'));

