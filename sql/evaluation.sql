--creating tables for best times

create table sos_time as select poly_id, min(time) as time from run, programme, solver where programme_id = programme.rowid and solver_id = solver.rowid and strategy = 'sos' and (status = 1 or verify = 1) group by poly_id;

create table best_sos as select run.rowid, verify, status, run.time, opt, run.poly_id, language, strategy, name from sos_time s, run, programme, solver where programme_id = programme.rowid and solver_id = solver.rowid and run.poly_id = s.poly_id and run.time = s.time;

create table sonc_time as select poly_id, min(time) as time from run, programme, solver where programme_id = programme.rowid and solver_id = solver.rowid and strategy = 'sonc' and (status = 1 or verify = 1) group by poly_id;

create table sonc_time_outer as select poly_id, min(time) as time from polynomial, run, programme, solver where polynomial.rowid = poly_id and programme_id = programme.rowid and solver_id = solver.rowid and strategy = 'sonc' and (status = 1 or verify = 1) and (params like '%outer%' or shape like '%simplex') group by poly_id;

create table best_sonc as select run.rowid, verify, status, run.time, opt, run.poly_id, language, strategy, name from sonc_time s, run, programme, solver where programme_id = programme.rowid and solver_id = solver.rowid and run.poly_id = s.poly_id and run.time = s.time;

create table best_sonc_outer as select run.rowid, verify, status, run.time, opt, run.poly_id, language, strategy, name from sonc_time_outer s, run, programme, solver where programme_id = programme.rowid and solver_id = solver.rowid and run.poly_id = s.poly_id and run.time = s.time;

create table best as select base.poly_id as poly_id, shape, variables, degree, terms, inner_terms, seed, verify_sonc, status_sonc, time_sonc, opt_sonc, language_sonc, name_sonc, best_sos.verify as verify_sos, best_sos.status as status_sos, best_sos.time as time_sos, best_sos.opt as opt_sos, best_sos.language as language_sos, best_sos.name as name_sos from (select polynomial.rowid as poly_id, shape, variables, degree, terms, inner_terms, seed, verify as verify_sonc, status as status_sonc, time as time_sonc, opt as opt_sonc, language as language_sonc, name as name_sonc from (polynomial left outer join best_sonc on polynomial.rowid = best_sonc.poly_id)) base left outer join best_sos on base.poly_id = best_sos.poly_id;

create table best_outer as select base.poly_id as poly_id, shape, variables, degree, terms, inner_terms, seed, verify_sonc, status_sonc, time_sonc, opt_sonc, language_sonc, name_sonc, best_sos.verify as verify_sos, best_sos.status as status_sos, best_sos.time as time_sos, best_sos.opt as opt_sos, best_sos.language as language_sos, best_sos.name as name_sos from (select polynomial.rowid as poly_id, shape, variables, degree, terms, inner_terms, seed, verify as verify_sonc, status as status_sonc, time as time_sonc, opt as opt_sonc, language as language_sonc, name as name_sonc from (polynomial left outer join best_sonc_outer on polynomial.rowid = best_sonc_outer.poly_id)) base left outer join best_sos on base.poly_id = best_sos.poly_id;

--restricting to solved instances
create table solved_run as select verify, status, time, opt, poly_id, programme_id, timestamp, system_id from run where status = 1 or verify = 1;

--restricting to existing non-trivial instances
create table interesting_poly as select * from polynomial where json != 'fail' and rowid not in (select poly_id from run where programme_id = 11);

create table best_interesting as select base.poly_id as poly_id, shape, variables, degree, terms, inner_terms, seed, degenerate_points, verify_sonc, status_sonc, time_sonc, opt_sonc, language_sonc, name_sonc, best_sos.verify as verify_sos, best_sos.status as status_sos, best_sos.time as time_sos, best_sos.opt as opt_sos, best_sos.language as language_sos, best_sos.name as name_sos from (select polynomial.rowid as poly_id, shape, variables, degree, terms, inner_terms, seed, degenerate_points, verify as verify_sonc, status as status_sonc, time as time_sonc, opt as opt_sonc, language as language_sonc, name as name_sonc from (interesting_poly polynomial left outer join best_sonc on polynomial.rowid = best_sonc.poly_id)) base left outer join best_sos on base.poly_id = best_sos.poly_id;

--creating tables for optimal values

create table sonc_opt as select poly_id, min(opt) as opt from run, programme, solver where programme_id = programme.rowid and solver_id = solver.rowid and strategy = 'sonc' and (status = 1 or verify = 1) group by poly_id;
create table sos_opt as select poly_id, min(opt) as opt from run, programme, solver where programme_id = programme.rowid and solver_id = solver.rowid and strategy = 'sos' and (status = 1 or verify = 1) group by poly_id;

create table opt_sonc as select run.rowid, verify, status, run.time, run.opt, run.poly_id, language, strategy, name from sonc_opt s, run, programme, solver where programme_id = programme.rowid and solver_id = solver.rowid and run.poly_id = s.poly_id and run.opt = s.opt and strategy = 'sonc';
create table opt_sos as select run.rowid, verify, status, run.time, run.opt, run.poly_id, language, strategy, name from sos_opt s, run, programme, solver where programme_id = programme.rowid and solver_id = solver.rowid and run.poly_id = s.poly_id and run.opt = s.opt and strategy = 'sos';

create table best_opt as select base.poly_id as poly_id, shape, variables, degree, terms, inner_terms, seed, verify_sonc, status_sonc, time_sonc, opt_sonc, language_sonc, name_sonc, best_sos.verify as verify_sos, best_sos.status as status_sos, best_sos.time as time_sos, best_sos.opt as opt_sos, best_sos.language as language_sos, best_sos.name as name_sos from (select polynomial.rowid as poly_id, shape, variables, degree, terms, inner_terms, seed, verify as verify_sonc, status as status_sonc, time as time_sonc, opt as opt_sonc, language as language_sonc, name as name_sonc from (polynomial left outer join best_sonc on polynomial.rowid = best_sonc.poly_id)) base left outer join best_sos on base.poly_id = best_sos.poly_id;

--compare even/variable split

create table ecos_even as select poly_id, time, opt, status, verify from run, programme, solver where programme_id = programme.rowid and solver_id = solver.rowid and name = 'ECOS' and params like '%even%' and (status = 1 or verify = 1);
create table ecos_variable as select poly_id, time, opt, status, verify from run, programme, solver where programme_id = programme.rowid and solver_id = solver.rowid and name = 'ECOS' and params like '%outer%' and (status = 1 or verify = 1);
create table ecos_split as select even.poly_id as poly_id, shape, variables, degree, terms, inner_terms, seed, even.time as time_even, even.opt as opt_even, even.status as status_even, even.verify as verify_even, ecos_variable.time as time_var, ecos_variable.opt as opt_var, ecos_variable.status as status_var, ecos_variable.verify as verify_var from (select * from polynomial left outer join ecos_even on ecos_even.poly_id = polynomial.rowid where shape = 'general') even left outer join ecos_variable on even.poly_id = ecos_variable.poly_id;

create table sedumi_even as select poly_id, time, opt, status, verify from run, programme, solver where programme_id = programme.rowid and solver_id = solver.rowid and name = 'SeDuMi' and params like '%even%' and (status = 1 or verify = 1);
create table sedumi_variable as select poly_id, time, opt, status, verify from run, programme, solver where programme_id = programme.rowid and solver_id = solver.rowid and name = 'SeDuMi' and params like '%outer%' and (status = 1 or verify = 1);
create table sedumi_split as select even.poly_id as poly_id, shape, variables, degree, terms, inner_terms, seed, even.time as time_even, even.opt as opt_even, even.status as status_even, even.verify as verify_even, sedumi_variable.time as time_var, sedumi_variable.opt as opt_var, sedumi_variable.status as status_var, sedumi_variable.verify as verify_var from (select * from polynomial left outer join sedumi_even on sedumi_even.poly_id = polynomial.rowid where shape = 'general') even left outer join sedumi_variable on even.poly_id = sedumi_variable.poly_id;

create table max_terms as select variables, degree, max(terms) from polynomial where json !='fail' and shape = 'general' group by variables, degree;
create table max_terms_solved as select variables, degree, max(terms) from polynomial, solved_run where json !='fail' and shape = 'general' and polynomial.rowid = solved_run.poly_id group by variables, degree;

--reading information
--requests for timing

select poly_id, shape, variables, degree, terms, inner_terms, seed, time_sonc, opt_sonc, name_sonc, time_sos, opt_sos, name_sos from best where time_sos < time_sonc;

--requests for the optimum
select count(*) from best_opt where opt_sos + 0.001 < opt_sonc;
select count(*) from best_opt where opt_sos is null and opt_sonc is not null;

--requests for splitting strategy
 select min(quot), max(quot), avg(quot) from (select time_even/time_var as quot from ecos_split where opt_even is not null and opt_var is not null);

