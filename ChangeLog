SONC-Algorithm (working title)

written by:
Henning Seidler

=== Version 0.1.0.0 ===

Date: 2018-02-12

Initial release

#Features 

* Read polynomials from string, as matrix/vector-pair or generate new random instances.
* Compute lower bounds for unconstraint polynomial minimization problems via various methods:
* *  SONC, using cvxpy and ECOS/SCS in Python
* *  SONC, using cvx and SeDuMi/SDPT3 in Matlab
* *  SOS, using cvxpy and SCS in Python
* *  SOS, using cvx and SeDuMi/SDPT3 in Matlab
* *  SOS, using SOStools in Matlab
* *  SOS, using Gloptipoly in Matlab
* all methods are accessed via a Python interface.
* obtain the decomposition into non-negative polynomials (see Known Issues).

#Known Issues

* Decomposition into squares will fail, if Gloptipoly/SOStools do not return a full-sized matrix, i.e. if some monomials do not appear in the solution.
* Decomposition as SONC not implemented for
* * p.sonc_opt_python()
* * p.opt(method='outer',*args)

=== Version 0.1.1.0 ===

Date: 2018-07-24

#Added Features

* Coefficients for solution written in sparse matrix/tensor
* First Version for reallocation of coefficients for negative points
* Greatly reduced number of variables in big GP

#Fixed Issues

* Decomposition for SONC works in all cases

#Known Issues

* Decomposition into squares will fail, if Gloptipoly/SOStools do not return a full-sized matrix, i.e. if some monomials do not appear in the solution.
* Reallocation of coefficients often fails, since ECOS tends to fail after some iterations

=== Version 0.1.1.1 ===

Date: 2019-01-11

#Fixed Issues

* Adjusted syntax for new versions of cvxpy and sympy

=== Version 0.2.0.0 ===

Date: 2019-01-30

#Added Features

* Computer lower bounds via SAGE
* Obtain rational lower bound in exact arithmetic; and decomposition with
rational coefficients in exact arithmetic, both via SONC and SAGE
* Compute local minimizers

#Noteable Changes

* Function calls for SONC unified
* separate class polynomial_base for input/output
* own classes for AGE- and circuit-polynomials

#Issues

* Matlab currently not available, was not tested in a while, may become
deprecated
* Decomposition for SOS will fail, if psd-matrix does not have full size, i.e. if some monomials do not appear in the solution.
* Symbolic decomposition of SAGE fails in many instances

=== Version 0.2.0.1 ===

Date: 2019-02-14

#Fixed Issues

* Some imports were missing in setup.py.
* When reading from string (or database), coefficients alsways had type "object".
* In detect_infinity, reduced digits for rounding from 23 to 7 (error happened, when going from decimal to binary).

=== Version 0.2.1.0 ===

Date: 2019-07-04

#Added Features

* Divided required packages into minimal/standard/full
* Additional flags for setup
* Compute minimiser of a Circuit Polynomial
* SONC/SAGE solvable with MOSEK 9
* Check nonnegativity exactly with Z3

#Other changes
* Use cdd as default solver for exact LP.
* rename Polynomial.symbolic() -> Polynomial.to_symbolic()

#Fixed Issues

* Specified Exceptions
* Changed some objects from numpy.matrix to numpy.array
* Added missing docstrings
* Removing some unused imports

=== Version 0.3.0.0 ===

Date: 2021-05-27

# Added Features

* Improved Lower Bounds via branch-and-bound
* alternative approach by running orthants in parallel

# Other changes

* removed SONC optimisation in Matlab
* removed dependency of polymake, directly uses cddlib
* aux.py has type annotations

# Fixed Issues

* Additional error handling
* Fixed Matlab bridge
* Clean-up of files

=== Version 0.3.0.0 ===

Date: 2021-08-25

#Fixed Issues

* fixed example in Readme
* handle MatlabError
